package org.example;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.util.*;
import java.util.stream.Collectors;

public class App
{

    // ---------------------------------------------------------------------------------------

    //Write and printf input data

    /* 2 - row
   j   - Oj - operation
   [0] - mj - machine
   [1] - pj - time of execution of element */

    private static int[][] writeInArrayWithTestData(int n) {
        int[][] input = new int[][]{{1, 2, 1, 2, 3, 1, 3, 1}, {7, 20, 5, 3, 10, 4, 8, 6}};

        return input;
    }

    public static void printfTwoDimensionalArray(int[][] input) {
        for (int i = 0; i < input.length; i++) {
            for (int j = 0; j < input[0].length; j++) {
                System.out.print(input[i][j] + "     ");
            }
            System.out.println();
        }
    }

    // ---------------------------------------------------------------------------------------

    // First C = Cmin
    //Cmin - Нижняя граница времени цикла
    private static Integer getCminFromInput(int[][] input) {

        List<Pair<Integer, Integer>> pairList = new ArrayList<>();
        for (int i = 0; i < input[0].length; i++) {
            Pair<Integer, Integer> pair = Pair.of(input[0][i], input[1][i]);
            pairList.add(pair);
        }

        //ключ = номер машины, значение - сумма времен циклов для этой машины
        Map<Integer, Integer> machineToSumOfCycleTimes = pairList.stream()
                .collect(Collectors.toMap(
                        pair -> pair.getKey(),
                        pair -> pair.getValue(),
                        (first, second) -> first + second)
                );

        return machineToSumOfCycleTimes.entrySet()
                .stream()
                .map(entry -> entry.getValue())
                .max(Integer::compareTo)
                .orElse(0);
    }

    // int[] ti - массив стартового времени выполнеия операций
    private static int[] getArrayStartTimeOperationsTjFromP(int[][] input) {

        int sizeArrayStartTimeOperations = input[1].length;
        int[] arrayStartTimeOperations = new int[sizeArrayStartTimeOperations];

        arrayStartTimeOperations[0] = 0;

        for (int i = 1; i < sizeArrayStartTimeOperations; i++) {
            arrayStartTimeOperations[i] = arrayStartTimeOperations[i - 1] + input[1][i - 1];
        }

        return arrayStartTimeOperations;
    }

//    --------------------------------- Алгоритм А ---------------------------------------------------

    //    --------------------------------- 1 -------------------------------------------------
    // kj - номер уровня, на котором начинается опреация Oj
    private static int[] getArrayLevelNumberKj(int[] arrayStartTimeOperations, int c) {
        int sizeArrayLevelNumber = arrayStartTimeOperations.length;
        int[] arrayLevelNumber = new int[sizeArrayLevelNumber];

        for (int i = 0; i < sizeArrayLevelNumber; i++) {
           arrayLevelNumber[i] = (arrayStartTimeOperations[i] / c) + 1;
//            System.out.println("i = " + i + " ; "
//                    + "kj = " + arrayLevelNumber[i]);
        }

        return arrayLevelNumber;
    }

    // В алгоритме : sj - время начала операции Oj некоторой детали
    // в промежутке [0, C], j = 1,...,n
    // В коде : si - время начала операции Oi некоторой детали
    //    // в промежутке [0, C], i = 0,...,n-1
    private static int[] getArrayBeginTimeOperationsSj(int[] arrayStartTimeOperations,
                                                       int[] arrayLevelNumber,
                                                       int c) {
        int sizeBeginTimeOperationsSj = arrayStartTimeOperations.length;
        int[] arrayBeginTimeOperationsSj = new int[sizeBeginTimeOperationsSj];

        for (int i = 0; i < sizeBeginTimeOperationsSj; i++) {
            arrayBeginTimeOperationsSj[i] = arrayStartTimeOperations[i] - ((arrayLevelNumber[i] -1)*c);
        }

        return  arrayBeginTimeOperationsSj;
    }
    //    ------------------------------------------------------------------------------------
    //    --------------------------------- 2 -------------------------------------------------


    //  Проверяем допустимость расписания
    private static int validitySchedule (int[][] input,
                                         int[] arrayLevelNumberKj,
                                         int[] arrayBeginTimeOperationsSj,
                                         int cycleTime) {
        int cycle = cycleTime;
        // Полагаем deltaMax = 0; Ti = 0; i = 1,...,m
        int deltaMax = 0;

        List<Pair<Integer, Integer>> beginnerTmj = getBeginnerTmj(input);
//        System.out.println("beginnerTmj = " + beginnerTmj);

//        List<Pair<Integer, Integer>> prevTml = getBeginnerTmj(input);


        List<Pair<Integer, Integer>> pairListJAndSj = getSortListJAndSj(arrayBeginTimeOperationsSj);
        System.out.println("pairListJAndSj = "+ pairListJAndSj);

        List<Pair<Integer, Pair<Integer, Integer>>> sortListMachineAndJAndSj = getSortListMachineAndJAndSj(arrayBeginTimeOperationsSj, input);
        System.out.println("sortListMachineAndJAndSj = "+ sortListMachineAndJAndSj);

        for (int i = 0; i < pairListJAndSj.size(); i++) {
            int Ti = (input[1][pairListJAndSj.get(i).getKey()] + pairListJAndSj.get(i).getValue()) % cycle;
            pairListJAndSj.get(i).getValue();

//            System.out.println("sj " + pairListJAndSj.get(i).getValue());
            int Tmj = input[1][pairListJAndSj.get(i).getKey()] + (pairListJAndSj.get(i).getValue()) % cycle;
            System.out.println(" Tmj = " + Tmj);

            int currentMachine = input[0][pairListJAndSj.get(i).getKey()];
//            System.out.println("current currentMachine = " +  currentMachine);

            int currentTmj = beginnerTmj.get(currentMachine - 1 ).getValue();
//            System.out.println("currentTmj = " + currentTmj);


////             if(sj < Tmj) then ... else ...
            System.out.println("s[" + pairListJAndSj.get(i).getKey()
                    + "] < Tm[" + pairListJAndSj.get(i).getKey() + "] = "
                    + pairListJAndSj.get(i).getValue() + " < " + currentTmj);
            if(pairListJAndSj.get(i).getValue() < currentTmj) {


                int iterPrevMachine = getIterPrevMachine(sortListMachineAndJAndSj, i, currentMachine);
                System.out.println("iter Prev Machine = " + iterPrevMachine);

                System.out.println("iter Current = " + pairListJAndSj.get(i).getKey());

                System.out.println("current pairListJAndSj[" + i + "]");

               beginnerTmj.get(currentMachine - 1).setValue(Tmj);

                //int[] arrayBeginTimeOperationsSj From index
//                System.out.println("TEST VARIABLE - Sj: "
//                        + arrayBeginTimeOperationsSj[iterPrevMachine]);
                int sl = arrayBeginTimeOperationsSj[iterPrevMachine];
                int sj = arrayBeginTimeOperationsSj[pairListJAndSj.get(i).getKey()];
                // p... From int[][] input[1][...]
//                System.out.println("TEST VARIABLE - Pj : "
//                        + input[1][iterPrevMachine]);
                int pl = input[1][iterPrevMachine];
                int pj = input[1][pairListJAndSj.get(i).getKey()];
                //int[] arrayLevelNumberKj From index
//                System.out.println("TEST VARIABLE - Kj : "
//                        + arrayLevelNumberKj[iterPrevMachine]);
                int kl = arrayLevelNumberKj[iterPrevMachine];
                int kj = arrayLevelNumberKj[pairListJAndSj.get(i).getKey()];
                //cycle - cycle

                int deltaCurrent = 0;
                deltaMax = getDeltaMax(cycle, deltaMax, pairListJAndSj, i, iterPrevMachine, sl, sj, pl, pj, kl, kj);

                System.out.println("\n");
            } else {
               beginnerTmj.get(currentMachine - 1).setValue(Tmj);
               System.out.println("\n");
            }
        }


    return deltaMax;
    }

    private static int getDeltaMax(int cycle, int deltaMax, List<Pair<Integer, Integer>> pairListJAndSj, int i, int iterPrevMachine, int sl, int sj, int pl, int pj, int kl, int kj) {
        int deltaCurrent;
        //1. if ((sl > sj) && (sj + pj > sl)) then deltaCurrent = (sj + pj - sl)/(kj - kl)
        //                                    if(deltaCurrent > deltaMax) {deltaMax = deltaCurrent;}
        if ((sl > sj) && (sj + pj > sl)) {
            deltaCurrent = (sj + pj - sl) / (kj - kl);
            System.out.println("--------------------------1------------------------------------");
            System.out.println("if ((s[" + iterPrevMachine
                    + "]  > s[" + pairListJAndSj.get(i).getKey()
                    + "] ) && (s[" + pairListJAndSj.get(i).getKey()
                    + "] + p[" + pairListJAndSj.get(i).getKey()
                    + "] > s[" + iterPrevMachine
                    + "]); then deltaCurrent = " + deltaCurrent);

            System.out.println("if (deltaCurrent = " + deltaCurrent
                    + " > deltaMax = " + deltaMax + ")");
            if (deltaCurrent > deltaMax) {
                deltaMax = deltaCurrent;

                System.out.println("  then deltaMax = " + deltaMax
                        + " when deltaCurrent = " + deltaCurrent);
            }
            System.out.println("--------------------------------------------------------------");
        }

        //2. if ((sl > sj) && (sl + pl - cycle > sj)) then deltaCurrent = (sj + pj - sl + с)/(kj - kl - 1)
        //                                    if(deltaCurrent > deltaMax) {deltaMax = deltaCurrent;}
        if ((sl > sj) && (sl + pl - cycle > sj)) {
            deltaCurrent = (sj + pj - sl + cycle)/(kj - kl - 1);
            System.out.println("-----------------------------2---------------------------------");
            System.out.println("if ((s[" + iterPrevMachine
                    + "]  > s[" + pairListJAndSj.get(i).getKey()
                    + "] ) && (s[" + iterPrevMachine
                    + "] + p[" + iterPrevMachine
                    + "] - cycle" + cycle
                    + "] > s[" + pairListJAndSj.get(i).getKey()
                    + "]); then deltaCurrent = " + deltaCurrent);

            System.out.println("if (deltaCurrent = " + deltaCurrent
                    + " > deltaMax = " + deltaMax + ")");
            if(deltaCurrent > deltaMax) {
                deltaMax = deltaCurrent;
                System.out.println("  then deltaMax = " + deltaMax
                        + " when deltaCurrent = " + deltaCurrent);
            }
            System.out.println("--------------------------------------------------------------");
        }
        //3. if ((sl <= sj) &&(sl + pl > sj)) then deltaCurrent = (sj + pj - sl)/(kj - kl)
        //                                    if(deltaCurrent > deltaMax) {deltaMax = deltaCurrent;}
        //if (l < j ) then existing
        //  else (j < l) then ???
        if ((sl <= sj) &&(sl + pl > sj)) {
            if (iterPrevMachine < pairListJAndSj.get(i).getKey() ) {
                deltaCurrent = (sj + pj - sl) / (kj - kl);
                System.out.println("-----------------------------3_1---------------------------------");
                System.out.println("if ((s[" + iterPrevMachine
                        + "]  <= s[" + pairListJAndSj.get(i).getKey()
                        + "] ) && (s[" + iterPrevMachine
                        + "] + p[" + iterPrevMachine
                        + "] > s[" + pairListJAndSj.get(i).getKey()
                        + "]); then deltaCurrent = " + deltaCurrent);

                System.out.println("if (deltaCurrent = " + deltaCurrent
                        + " > deltaMax = " + deltaMax + ")");
                if (deltaCurrent > deltaMax) {
                    deltaMax = deltaCurrent;

                    System.out.println("  then deltaMax = " + deltaMax
                            + " when deltaCurrent = " + deltaCurrent);
                }
                System.out.println("--------------------------------------------------------------");
            }
            if (pairListJAndSj.get(i).getKey() < iterPrevMachine) {
                deltaCurrent = (sl + pl - sj) / (kl - kj);
                System.out.println("-----------------------------3_2---------------------------------");
                System.out.println("if ((s[" + iterPrevMachine
                        + "]  <= s[" + pairListJAndSj.get(i).getKey()
                        + "] ) && (s[" + iterPrevMachine
                        + "] + p[" + iterPrevMachine
                        + "] > s[" + pairListJAndSj.get(i).getKey()
                        + "]); then deltaCurrent = " + deltaCurrent);

                System.out.println("if (deltaCurrent = " + deltaCurrent
                        + " > deltaMax = " + deltaMax + ")");
                if (deltaCurrent > deltaMax) {
                    deltaMax = deltaCurrent;

                    System.out.println("  then deltaMax = " + deltaMax
                            + " when deltaCurrent = " + deltaCurrent);
                }
                System.out.println("--------------------------------------------------------------");
            }
        }
        //4. if ((sl <= sj) &&(sj + pj - cycle > sl)) then deltaCurrent = (sj + pj - sl - cycle)/(kj - kl + 1)
        //                                    if(deltaCurrent > deltaMax) {deltaMax = deltaCurrent;}
        if ((sl <= sj) && (sj + pj - cycle > sl)) {
            deltaCurrent = (sj + pj - sl - cycle)/(kj - kl + 1);

            System.out.println("------------------------4--------------------------------------");
            System.out.println("if ((s[" + iterPrevMachine
                    + "]  <= s[" + pairListJAndSj.get(i).getKey()
                    + "] ) && (s[" + pairListJAndSj.get(i).getKey()
                    + "] + p[" + pairListJAndSj.get(i).getKey()
                    + "] - cycle " + cycle
                    + ") > s[" + iterPrevMachine
                    + "]); then deltaCurrent = " + deltaCurrent);

            System.out.println("if (deltaCurrent = " + deltaCurrent
                    + " > deltaMax = " + deltaMax + ")");
            if(deltaCurrent > deltaMax) {
                deltaMax = deltaCurrent;
                System.out.println("  then deltaMax = " + deltaMax
                        + " when deltaCurrent = " + deltaCurrent);
            }
            System.out.println("--------------------------------------------------------------");
        }
        return deltaMax;
    }

    private static int getIterPrevMachine(List<Pair<Integer, Pair<Integer, Integer>>> sortListMachineAndJAndSj, int i, int currentMachine) {
        int iterPrevMachine = - 1;
        while (iterPrevMachine == -1) {
            for (int j = i - 1; j>=0; j--) {
                if (currentMachine == sortListMachineAndJAndSj.get(j).getKey()) {
                    System.out.println("sortListMachineAndJAndSj.get(j) = " + sortListMachineAndJAndSj.get(j));
                    iterPrevMachine = sortListMachineAndJAndSj.get(j).getValue().getKey();
                    break;

                }
            }
        }
        return iterPrevMachine;
//                System.out.println("iterPrevMachine = " + iterPrevMachine);
    }

    // Перебираем операции Oj в порядке неубывания sj
    // List<Pair<J, Sj>>
    private static List<Pair<Integer, Integer>> getSortListJAndSj(int[] arrayBeginTimeOperationsSj) {
        List<Pair<Integer, Integer>> pairListJAndSj = new ArrayList<>();
        for (int i = 0; i < arrayBeginTimeOperationsSj.length; i++) {
            Pair<Integer, Integer> pair = Pair.of(i, arrayBeginTimeOperationsSj[i]);
            pairListJAndSj.add(pair);
        }

        Collections.sort(pairListJAndSj, Comparator.comparing(Pair::getValue));
        return pairListJAndSj;
    }

    private static List<Pair<Integer, Pair<Integer, Integer>>> getSortListMachineAndJAndSj(int[] arrayBeginTimeOperationsSj,
                                                                            int[][] input) {
        // List<Pair<Machine, Pair<J, SJ>>>
        List<Pair<Integer, Pair<Integer, Integer>>> pairListMachineAndJAndSj = new ArrayList<>();

        List<Pair<Integer, Integer>> pairSortListJAndSj = getSortListJAndSj(arrayBeginTimeOperationsSj);

        for (int i = 0; i < pairSortListJAndSj.size(); i++) {
            Pair<Integer, Pair<Integer, Integer>> pairPair = Pair.of(input[0][pairSortListJAndSj.get(i).getKey()],
            pairSortListJAndSj.get(i));
            pairListMachineAndJAndSj.add(pairPair);
        }
        return  pairListMachineAndJAndSj;
    }

    private static List<Pair<Integer, Integer>> getBeginnerTmj(int[][] ints) {
        Set<Integer> machines = new HashSet<Integer>();
        for (int i = 0; i < ints[0].length; i++) {
            machines.add(ints[0][i]);
        }

        int[] arrayMachines = new int[machines.size()];
        Iterator<Integer> i = machines.iterator();
        for (int j = 0; j < machines.size(); j++) {
            arrayMachines[j] = i.next();
        }


        List<Pair<Integer, Integer>> pairListTmj = new ArrayList<>();
        for (int l = 0; l < arrayMachines.length; l++) {
            Pair<Integer, Integer> pair = MutablePair.of(arrayMachines[l], 0);
            pairListTmj.add(pair);
        }


        return pairListTmj;
    }

    //    ------------------------------------------------------------------------------------
    //    --------------------------------------------------------------------------------------------

    //todo return type data - ! int ! = last C - the optimal cycle time.
    private static void init (int[][] input) {
        int deltaMax;

        int c = getCminFromInput(input);
        int[] arrayStartTimeOperationsFromP = getArrayStartTimeOperationsTjFromP(input);

        do {
            int[] arrayLevelNumberKj = getArrayLevelNumberKj(arrayStartTimeOperationsFromP, c);
            int[] arrayBeginTimeOperationsSj = getArrayBeginTimeOperationsSj(arrayStartTimeOperationsFromP,
                arrayLevelNumberKj,
                c);

            deltaMax = validitySchedule(input, arrayLevelNumberKj, arrayBeginTimeOperationsSj, c);
            System.out.println("deltaMax = " + deltaMax);
            c = c + deltaMax;
            System.out.println("cycle = " + c);
//            c = c + 9; // second iteration

        } while (deltaMax > 0);


    //todo return c;
    }
    //    --------------------------------------------------------------------------------------------

    public static void main(String[] args) {
        int n = 8;
        int[][] arrayWithTestData = writeInArrayWithTestData(n);
        printfTwoDimensionalArray(arrayWithTestData);
////    ------------------------------------------------------------------------------------
        init(arrayWithTestData);
////    ------------------------------------------------------------------------------------

//        Integer cminFromInput = getCminFromInput(arrayWithTestData);
//        System.out.println("Cmin = " + cminFromInput);
//
//        int[] arrayStartTimeOperationsFromP = getArrayStartTimeOperationsTjFromP(arrayWithTestData);
//        System.out.println("int[] ti = " + Arrays.toString(arrayStartTimeOperationsFromP));
//
//        int[] arrayLevelNumberKj = getArrayLevelNumberKj(arrayStartTimeOperationsFromP, cminFromInput);
//        System.out.println("int[] Kj = " + Arrays.toString(arrayLevelNumberKj));
//
//        int[] arrayBeginTimeOperationsSj = getArrayBeginTimeOperationsSj(arrayStartTimeOperationsFromP,
//                arrayLevelNumberKj,
//                cminFromInput);
//        System.out.println("int[] Sj = " + Arrays.toString(arrayBeginTimeOperationsSj));

//        int validitySchedule = validitySchedule(arrayWithTestData,
//                arrayBeginTimeOperationsSj,
//                cminFromInput);
//        System.out.println("validitySchedule = " + validitySchedule);
//    ------------------------------------------------------------------------------------


//        for (int i = 0; i < arrayWithTestData[1].length; i++) {
//            System.out.print("P[" + i + "] = "+ arrayWithTestData[1][i] + "; ");
//        }
//        System.out.print("\n");
//
//        Integer cminFromInput = getCminFromInput(arrayWithTestData);
//        int[] arrayStartTimeOperationsFromP = getArrayStartTimeOperationsTjFromP(arrayWithTestData);
//
//        int[] arrayLevelNumberKj = getArrayLevelNumberKj(arrayStartTimeOperationsFromP, cminFromInput);
//        for (int i = 0; i < arrayLevelNumberKj.length; i++) {
//            System.out.print("K[" + i + "] = "+ arrayLevelNumberKj[i] + "; ");
//        }
//        System.out.print("\n");
//
//        int[] arrayBeginTimeOperationsSj = getArrayBeginTimeOperationsSj(arrayStartTimeOperationsFromP,
//                arrayLevelNumberKj,
//                cminFromInput);
//        for (int i = 0; i < arrayBeginTimeOperationsSj.length; i++) {
//            System.out.print("S[" + i + "] = "+ arrayBeginTimeOperationsSj[i] + "; ");
//        }
//        System.out.print("\n");
//
//        int validitySchedule = validitySchedule(arrayWithTestData,
//                arrayLevelNumberKj,
//                arrayBeginTimeOperationsSj,
//                cminFromInput);
//        //    ------------------------------------------------------------------------------------
    }
}
